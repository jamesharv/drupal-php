#!/bin/bash
set -xe

# PHP Environment variables.
ENV_FILE=/etc/php5/fpm/pool.d/www-environment.conf
echo '[www]' > $ENV_FILE

for var in $PHP_ENV_VARS; do
  echo "env[$var]=\$$var" >> $ENV_FILE
done

# Disable xdebug and xhprof by default.
for ext in xdebug xhprof; do
  php5dismod $ext
done

# Enable user-defined extensions.
for ext in $PHP_EXTENSIONS; do
  php5enmod $ext
done

/usr/sbin/php5-fpm --nodaemonize $@
